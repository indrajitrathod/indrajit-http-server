const express = require('express');
const app = express();
const { v4: uuidv4 } = require('uuid');
const status = require('http-status');
const port = 3000;

app.get('/html', (req, res) => {
    res.sendFile(`${__dirname}/src/html_file.html`);
});

app.get('/json', (req, res) => {
    res.sendFile(`${__dirname}/src/json_file.json`);
});

app.get('/uuid', (req, res) => {
    res.send(uuidv4());
});

app.get('/status/:status_code', (req, res) => {
    if (isNaN(req.params.status_code) || !status[req.params.status_code]) {
        res.sendStatus(422);
    } else {
        res.sendStatus(req.params.status_code);
    }
});

app.get('/delay/:delay_time', (req, res) => {
    if (isNaN(req.params.delay_time) || req.params.delay_time < 0) {
        res.sendStatus(422);
    } else {
        setTimeout(() => {
            res.sendStatus(200);
        }, req.params.delay_time * 1000);
    }
});

app.listen(port, () => {
    console.log('Server ready');
});
